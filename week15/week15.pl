use strict;
use warnings;

my %grades = ("Ahmet" => 80, "Zeynep" => 50, "Kemal" => 100);

print "Sort by key - alphabetical order\n";
foreach my $student (sort keys %grades){
	print "$student $grades{$student}\n";
}

print "Sort by value - numerical order\n";
foreach my $student (sort {$grades {$b} <=> $grades {$a} }keys %grades){
	print "$student $grades{$student}\n";
}

#Two dimensional array example

my @a = ([1,2,3],[4,5,6],[7,8,9]);

print "$a[2][1] $a[1][2]   $a[0]\n";

my $row = scalar @a;
my $cols = scalar @{$a[0]};

for (my $i = 0; $i < $row; $i++){
	for (my $j = 0; $j < $cols; $j++){
		print "$a[$i][$j] "; #in order to calculate tranpose of a matrix simply change the order a[$j][$i]
	}
	print "\n";
}

#array of hash example

my @AoH = ({husband => "barney", wife => "betty", son => "bam bam" },
		   {husband => "george", wife => "jane", son => "elroy"},
		   {husband => "homer", wife => "marge", son => "bart"});

my %hash = %{$AoH[1]};		   
print "${$AoH[1]}{wife}\n";	
print "$hash{wife}\n"	;   

foreach $element (@AoH){
		my %tmp_hash = %{$element};
		
		foreach $key (sort keys %tmp_hash){
			print "$key: $tem_hash{$key}\t";
		}
		print "\n";
}
		   
		   
		   